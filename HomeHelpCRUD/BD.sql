-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.28-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for homehelp
CREATE DATABASE IF NOT EXISTS `homehelp` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_bin */;
USE `homehelp`;

-- Dumping structure for table homehelp.correo
CREATE TABLE IF NOT EXISTS `correo` (
  `idcorreo` int(11) NOT NULL AUTO_INCREMENT,
  `User` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `Pass` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `Nombre` varchar(50) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`idcorreo`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table homehelp.correo: ~7 rows (approximately)
DELETE FROM `correo`;
/*!40000 ALTER TABLE `correo` DISABLE KEYS */;
INSERT INTO `correo` (`idcorreo`, `User`, `Pass`, `Nombre`) VALUES
	(1, 'luis@gmail.com', '1234', '0'),
	(3, 'luis3@gmail.com', '6678', '0'),
	(4, 'aber@gmail.com', '890', '0'),
	(5, 'hm@gmail,com', '', '0'),
	(6, 'hm2@gmail,com', '', '0'),
	(7, 'hm3@gmail,com', '9999', '0'),
	(9, 'ola@gmail.com', '345', 'Luis');
/*!40000 ALTER TABLE `correo` ENABLE KEYS */;

-- Dumping structure for table homehelp.listas
CREATE TABLE IF NOT EXISTS `listas` (
  `id_lista` int(11) NOT NULL AUTO_INCREMENT,
  `nom_lista` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `prod` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id_lista`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table homehelp.listas: ~0 rows (approximately)
DELETE FROM `listas`;
/*!40000 ALTER TABLE `listas` DISABLE KEYS */;
/*!40000 ALTER TABLE `listas` ENABLE KEYS */;

-- Dumping structure for table homehelp.producto
CREATE TABLE IF NOT EXISTS `producto` (
  `id_producto` int(11) NOT NULL AUTO_INCREMENT,
  `nom_producto` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `tipo_producto` varchar(80) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `status` int(11) DEFAULT '1',
  PRIMARY KEY (`id_producto`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table homehelp.producto: ~38 rows (approximately)
DELETE FROM `producto`;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` (`id_producto`, `nom_producto`, `tipo_producto`, `status`) VALUES
	(1, 'paprika', 'Saborizantes', 1),
	(2, 'Pan Blanco', 'Panaderia', 0),
	(3, 'Spaghetti', 'Cereales', 0),
	(4, 'FortiLeche', 'Lácteos', 1),
	(5, 'Gatorade', 'Bebidas', 1),
	(6, 'Muñon', 'Carnes frias', 1),
	(7, 'Papas Fritas', 'Congelados', 0),
	(8, 'Salsa Valentina', 'Aderezos', 1),
	(9, 'Fabuloso', 'Limpieza', 1),
	(10, 'Atún', 'Conservas', 1),
	(11, 'Zucaritas', 'Cereales', 1),
	(12, 'Cotonetes', 'Hogar', 1),
	(13, 'Manzanas', 'Frutas y verduras', 1),
	(14, 'Salchicha', 'Carnes', 0),
	(15, 'Cuernitos', 'Panaderia', 1),
	(16, 'Sabritas', 'Botanas', 1),
	(17, 'Jugo Jumex', 'Bebidas', 1),
	(18, 'Espinaca', 'Frutas y verduras', 1),
	(19, 'Cuernos', 'Panaderia', 1),
	(20, 'Del Valle', 'Bebidas', 1),
	(21, 'Hojaldras', 'Panaderia', 1),
	(23, 'Conchas', 'Panaderia', 1),
	(27, 'Powerade', 'Bebidas', 1),
	(28, 'Carne Molida', 'Congelados', 1),
	(29, 'Cloro', 'Hogar', 1),
	(30, 'Espinaca', 'Frutas y verduras', 0),
	(31, 'Chicharos', 'Enlatados', 1),
	(32, 'Pan molido', 'Granos y harinas', 1),
	(33, 'Salchicha', 'Carnes frias', 1),
	(34, 'Cigarros', 'Personal', 1),
	(35, 'Corn Flakes', 'Cereales', 1),
	(36, 'chorizo', 'Carnes', 1),
	(37, 'Lapiz Labial', 'Cosméticos', 1),
	(38, 'Mayonesa Mackornic', 'Condimentos', 1),
	(39, 'mozarella', 'quesos', 1),
	(40, 'pan', 'pan', 1),
	(41, '', 'Prro', 1),
	(42, '3', 'Prro', 1),
	(43, '3', 'prro', 1),
	(44, 'paprika', 'Saborizantes', 1);
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;

-- Dumping structure for table homehelp.servicio
CREATE TABLE IF NOT EXISTS `servicio` (
  `id_servicio` int(11) NOT NULL AUTO_INCREMENT,
  `nom_servicio` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `desc_servicio` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `pago_servicio` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `tipo_servicio` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `tipo_pago` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_servicio`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- Dumping data for table homehelp.servicio: ~18 rows (approximately)
DELETE FROM `servicio`;
/*!40000 ALTER TABLE `servicio` DISABLE KEYS */;
INSERT INTO `servicio` (`id_servicio`, `nom_servicio`, `desc_servicio`, `pago_servicio`, `tipo_servicio`, `tipo_pago`, `status`) VALUES
	(1, 'netflix', 'pagar netflix para ver stranger things', '360', 'Entretenimiento', 'Entretenimiento', 1),
	(2, 'Agua', 'Pagar el Agua', '780', 'Básico', 'Básico', 0),
	(3, 'luz', 'pagar la luz', '500', 'Básico', 'Básico', 1),
	(4, 'Agua', 'mucha agua', '700', 'Básico', 'Básico', 1),
	(5, 'spotify', 'tengo musho dinero', '50', 'Entretenimiento', 'Entretenimiento', 1),
	(6, 'Cable', 'pagar el IZZI', '360', 'Entretenimiento', 'Entretenimiento', 1),
	(7, 'Internet', 'El intenet', '590', 'Básico', 'Básico', 1),
	(8, 'Telefono', 'Linea de Telefono', '400', 'Entretenimiento', 'Entretenimiento', 1),
	(9, 'Celular', 'Pago del cel', '690', 'Entretenimiento', 'Entretenimiento', 1),
	(10, 'Celular Mama', 'Pagar el Cel de Mama', '900', 'Entretenimiento', 'Entretenimiento', 1),
	(11, 'Celular Papa', 'Pagar el Cel de Papa', '1100', 'Entretenimiento', 'Entretenimiento', 1),
	(12, 'Celular Yo', 'yo', '120', 'Entretenimiento', 'Entretenimiento', 1),
	(13, 'Restaurant', 'Pagar Cuenta', '503', 'Entretenimiento', 'Entretenimiento', 0),
	(14, 'Celulares', 'mas cels', '790', 'Entretenimiento', 'Entretenimiento', 1),
	(15, 'Memes', 'Recien Horneados', '690', 'Entretenimiento', 'Entretenimiento', 1),
	(16, 'Cels', 'mas aun', '340', 'Básico', 'Básico', 1),
	(17, 'Joshua', 'xd', '345', 'Básico', 'Básico', 1),
	(18, 'Inscripcion Ut', 'Pago de Inscripcion', '1700', 'Básico', 'Básico', 1);
/*!40000 ALTER TABLE `servicio` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
