package com.example.luise.homehelp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Eliminar_producto extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eliminar_producto);
}
    public void servicios(View vi){
        Intent i= new Intent(this,Servicios.class);
        startActivity(i);
    }
    public void despensas( View i) {
        Intent e = new Intent(this, Despensas.class);
        startActivity(e);
    }
    public void cancelar (View vir){
        Intent k= new Intent (this,MenuPrincipal.class);
        startActivity(k);
    }
}
