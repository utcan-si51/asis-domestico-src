package com.example.luise.homehelp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Eliminar_Servicio extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eliminar__servicio);
    }
    public void despensas( View i) {
        Intent e = new Intent(this, Despensas.class);
        startActivity(e);
    }
    public void cancelar (View vi){
        Intent k= new Intent (this,MenuPrincipal.class);
        startActivity(k);
    }
}
