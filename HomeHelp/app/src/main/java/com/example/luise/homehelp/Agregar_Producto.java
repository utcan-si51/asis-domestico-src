package com.example.luise.homehelp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Agregar_Producto extends AppCompatActivity  {

    Spinner spprods;
    String[] strprods;
    List<String> listaprods;
    ArrayAdapter<String> comboAdapter;
    String nombreprod;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar__producto);
        //================Datos cargados desde Array=====================//
        //Hago referencia al spinner con el id `sp_frutas`
        spprods = (Spinner) findViewById(R.id.sp_prods);
        //Implemento el setOnItemSelectedListener: para realizar acciones cuando se seleccionen los ítems

        //Convierto la variable List<> en un ArrayList<>()
        listaprods = new ArrayList<>();
        //Arreglo con nombre de frutas
        strprods = new String[]{"Pan", "Lacteos", "Jugos", "Especias", "Frutas","Verduras","Enlatados"};
        //Agrego las frutas del arreglo `strFrutas` a la listaFrutas
        Collections.addAll(listaprods, strprods);
        //Implemento el adapter con el contexto, layout, listaFrutas
        comboAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, listaprods);
        //Cargo el spinner con los datos
        spprods.setAdapter(comboAdapter);
    }

    public void servicios(View vi){
        Intent i= new Intent(this,Servicios.class);
        startActivity(i);
    }
    public void despensas( View i) {
        Intent e = new Intent(this, Despensas.class);
        startActivity(e);
    }
    public void cancelar (View vi){
        Intent k= new Intent (this,MenuPrincipal.class);
        startActivity(k);
    }
}

