package com.example.luise.homehelp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Crear_lista extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_lista);
    }
    public void servicios(View vi){
        Intent i= new Intent(this,Servicios.class);
        startActivity(i);
    }
    public void despensas( View i) {

        Intent e = new Intent(this, Despensas.class);
        startActivity(e);
    }

    public void ag_prod(View v){
        Intent i= new Intent(this,Agregar_Producto.class);
        startActivity(i);

    }
    public void cancelar (View vir){
        Intent k= new Intent (this,MenuPrincipal.class);
        startActivity(k);
    }
}
