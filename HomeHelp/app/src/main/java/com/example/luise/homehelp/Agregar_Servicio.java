package com.example.luise.homehelp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Agregar_Servicio extends AppCompatActivity {

    Spinner spserv,spserv2;
    String[] strserv,strserv2;
    List<String> listaserv,listaserv2;
    ArrayAdapter<String> comboAdapter,comboAdapter2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar__servicio);
        //================Datos cargados desde Array=====================//
        //Hago referencia al spinner con el id `sp_frutas`
        spserv = (Spinner) findViewById(R.id.spserv);
        //Implemento el setOnItemSelectedListener: para realizar acciones cuando se seleccionen los ítems

        //Convierto la variable List<> en un ArrayList<>()
        listaserv = new ArrayList<>();
        //Arreglo con nombre de frutas
        strserv = new String[]{"Entretenimiento","Basico"};
        //Agrego las frutas del arreglo `strFrutas` a la listaFrutas
        Collections.addAll(listaserv, strserv);
        //Implemento el adapter con el contexto, layout, listaFrutas
        comboAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, listaserv);
        //Cargo el spinner con los datos
        spserv.setAdapter(comboAdapter);

        //================Datos cargados desde Array=====================//
        //Hago referencia al spinner con el id `sp_frutas`
        spserv2 = (Spinner) findViewById(R.id.spserv2);
        //Implemento el setOnItemSelectedListener: para realizar acciones cuando se seleccionen los ítems

        //Convierto la variable List<> en un ArrayList<>()
        listaserv2 = new ArrayList<>();
        //Arreglo con nombre de frutas
        strserv2 = new String[]{"Anual", "Mensual"};
        //Agrego las frutas del arreglo `strFrutas` a la listaFrutas
        Collections.addAll(listaserv2, strserv2);
        //Implemento el adapter con el contexto, layout, listaFrutas
        comboAdapter2 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, listaserv2);
        //Cargo el spinner con los datos
        spserv2.setAdapter(comboAdapter2);
    }
    public void despensas(View view){
        Intent i= new Intent(this,Despensas.class);
        startActivity(i);
    }
    public void cancelar (View vi){
        Intent k= new Intent (this,MenuPrincipal.class);
        startActivity(k);
    }
}
