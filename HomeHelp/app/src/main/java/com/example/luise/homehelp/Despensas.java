package com.example.luise.homehelp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Despensas extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_despensas);
    }
    public void servicios(View vi){
        Intent i= new Intent(this,Servicios.class);
        startActivity(i);
    }

    public void ag_prod(View v){
        Intent i= new Intent(this,Agregar_Producto.class);
        startActivity(i);

    }
    public void cr_list(View w){
        Intent i=new Intent(this,Crear_lista.class);
        startActivity(i);
    }
}
