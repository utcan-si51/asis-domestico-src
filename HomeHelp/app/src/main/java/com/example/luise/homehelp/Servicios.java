package com.example.luise.homehelp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Servicios extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servicios);
    }
    public void despensas(View view){
        Intent i= new Intent(this,Despensas.class);
        startActivity(i);
    }
    public void agr_serv(View vi){
        Intent i= new Intent(this,Agregar_Servicio.class);
        startActivity(i);
    }
    public void elim_serv(View vie){
        Intent e= new Intent(this,Eliminar_Servicio.class);
        startActivity(e);
    }

}
